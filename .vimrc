"
" UI Config
"

set encoding=utf-8

" Wertol personal
if has("unix")
	set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 9
elseif has("windows")
	set guifont=Consolas\ for\ Powerline\ FixedD:h9
endif

colors inkpot
set lines=55 columns=140
set textwidth=80
set colorcolumn=80

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

set number       " show line numbers
set showcmd      " show command in bottom bar
set cursorline   " highlight current line
set ruler        " Always show current position
"set cmdheight=2  " Height of the command bar
set wildmenu     " visual autocomplete for command menu
set lazyredraw	 " Don't redraw while executing macros (good performance config)
set showmatch    " highlight matching [{()}]

"
" Searching
"
set incsearch " Makes search act like search in modern browsers
set magic     " For regular expressions turn magic on

"
" Other Settings
"

set autoread   " Set to auto read when a file is changed from the outside
set ignorecase " Ignore case when searching
set smartcase  " When searching try to be smart about cases
set hlsearch   " Highlight search results
set mat=5      " How many tenths of a second to blink when matching brackets

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

set smarttab " Be smart when using tabs ;)

set ai   " Auto indent
set si   " Smart indent
"set wrap " Wrap lines

set nocompatible
filetype off

if has("unix")
	set rtp+=~/.vim/bundle/vundle
	call vundle#begin()
elseif has("windows")
	set rtp+=~/vimfiles/bundle/Vundle.vim/
	let path='~/vimfiles/bundle'
	call vundle#begin(path)
endif

Bundle 'gmarik/vundle'
"Bundle 'Valloric/YouCompleteMe'
Bundle 'Valloric/ListToggle'
Bundle 'scrooloose/syntastic'
"Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Bundle 'nathanaelkane/vim-indent-guides'
Bundle 'ntpeters/vim-better-whitespace'
"Bundle 'git://git.code.sf.net/p/vim-latex/vim-latex'
Bundle 'https://github.com/scrooloose/nerdtree'
Bundle 'bling/vim-airline'

call vundle#end()

" Enable filetype plugins
filetype plugin indent on

let mapleader = ","

" Automatically switches on removal of whitespaces on saving.
autocmd VimEnter * ToggleStripWhitespaceOnSave
" Switch on whitespace coloring.
autocmd VimEnter * IndentGuidesEnable

" Press Space to turn off highlighting and clear any message already displayed.
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

set laststatus=2
let g:Powerline_symbols="fancy"

" vim-airline settings
let g:airline#extensions#tabline#enabled = 1
"let g:airline_powerline_fonts = 1

let g:airline_symbols = {}
let g:airline_left_sep = "\u2b80" "use double quotes here
let g:airline_left_alt_sep = "\u2b81"
let g:airline_right_sep = "\u2b82"
let g:airline_right_alt_sep = "\u2b83"
let g:airline_symbols.branch = "\u2b60"
let g:airline_symbols.readonly = "\u2b64"
let g:airline_symbols.linenr = "\u2b61"

autocmd vimenter * NERDTree

" Split navigation remap
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"hi IndentGuidesOdd  guibg=red   ctermbg=3
"hi IndentGuidesEven guibg=green ctermbg=4

syntax enable           " enable syntax processing
"set omnifunc=syntaxcomplete#Complete

" Link section
"   * Powerline Font: http://www.codejury.com/consolas-font-in-vim-powerline-windows/
"   *


